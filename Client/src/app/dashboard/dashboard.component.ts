import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public userData: any[];
  public formGroup: FormGroup;
  private editedRowIndex: number;

  constructor(private dataservice: DataService) { }

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.dataservice.getUsers().subscribe(Data => {
      this.userData = Data.users;
    });
  }

  removeHandler({ dataItem }) {
    this.dataservice.removeUser(dataItem).subscribe((data) => {
      this.userData.splice(this.userData.indexOf(dataItem), 1);
    });
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  editHandler({ sender, rowIndex, dataItem }) {

    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      'firstName': new FormControl(dataItem.firstName, Validators.required),
      'lastName': new FormControl(dataItem.lastName, Validators.required),
      '_id': new FormControl(dataItem._id)
    });

    this.editedRowIndex = rowIndex;

    sender.editRow(rowIndex, this.formGroup);

  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    const user: any = formGroup.value;

    if (isNew) {

      this.dataservice.addUser(user).subscribe((data) => {
        this.userData.push(data.user);
      });

    } else {

      this.userData[rowIndex].firstName = user.firstName;
      this.userData[rowIndex].lastName = user.lastName;

      this.dataservice.updateUser(user).subscribe((data) => { });
    }

    sender.closeRow(rowIndex);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public addHandler({ sender }) {

    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      'firstName': new FormControl('', Validators.required),
      'lastName': new FormControl('', Validators.required),
    });

    sender.addRow(this.formGroup);
  }

}
