import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getUsers() {
    return this.http.get('http://localhost:3000/api/users')
           .map((response) => response.json());
  }

  addUser(dataItem) {

    const body = {
      'firstName' : dataItem.firstName,
      'lastName' : dataItem.lastName
    };

    return this.http.post('http://localhost:3000/api/add', body)
        .map((response) => response.json());
  }

  removeUser(dataItem) {
    return this.http.delete('http://localhost:3000/api/delete/' + dataItem._id)
        .map((response) => response.json());
  }

  updateUser(dataItem) {
    const body = {
      'firstName' : dataItem.firstName,
      'lastName' : dataItem.lastName
    };
    return this.http.post('http://localhost:3000/api/update/' + dataItem._id, body)
        .map((response) => response.json());
  }
}
