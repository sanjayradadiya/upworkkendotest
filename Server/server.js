var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.connect('mongodb://admin:admin@ds111420.mlab.com:11420/upworkkendotest');
var User = require('./models/User');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type,image/gif');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

var port = process.env.PORT || 3000;
var router = express.Router();

router.route('/add').post(function (req, res) {
  user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName
  });

  user.save(function (err, data) {
    if (err) {
      res.json(err)
    } else {
      res.json({
        message: 'User Created',
        user: data
      });
    }
  });
})

router.route('/users').get(function (req, res) {
  User.find({}, function (err, data) {
    if (err) {
      throw err;
    } else {
      res.json({
        users: data
      });
    }
  });
});

router.route('/update/:id').post(function (req, res) {
  User.findByIdAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
      }
    },
    function (err, data) {
      if (err) {
        res.send(err);
      } else {
        res.json({
          message: 'Product updated!',
          user: data
        });
      }
    });

});

router.route('/delete/:user_id').delete(function (req, res) {
  User.remove({
    _id: req.params.user_id
  }, function (err, user) {
    if (err) {
      res.send(err);
    }
    res.json({
      msg: 'succesfully deleted'
    })
  });
});


app.use('/api', router);

app.listen(port);
console.log('connected on port ' + port);