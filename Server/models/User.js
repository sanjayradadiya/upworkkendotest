var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = Schema({
  firstName: String,
  lastName: String,
});

module.exports = mongoose.model('User',userSchema);